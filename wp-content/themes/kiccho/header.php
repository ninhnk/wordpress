<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kiccho
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <!-- GOOGLE API -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXvS7gzad57RVT8CH8V4kQBHa2xOqNumM&callback=initMap&language=ja&region=JP"
    defer></script>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="top-page">
    <div class="main-banner">
        <header>
            <button class="nav-menu">
                <span class="bar1"></span>
                <span class="bar2"></span>
                <span class="bar3"></span>
                <p>menu</p>
            </button>
            <div class="open-nav">
                <div class="content-nav">
                    <div class="logo-black logo-inner-nav">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>">
                            <img src="<?php echo get_template_directory_uri() .'/assets/images/logo_black.png' ?>" alt="<?php bloginfo( 'name' ); ?>">
                        </a>
                    </div>

                    <div class="text-style text-inner-nav">
                        <a href="#">お問い合わせ</a>
                        <a href="#">会社概要</a>
                        <a href="#">キモノクリニック</a>
                        <a href="#">小物通販</a>
                        <a href="#">取扱商品</a>
                        <a href="#">振袖特設ペ<span>ー</span>ジ</a>
                        <a href="#">お知らせ</a>
                        <a href="#">メッセ<span>ー</span>ジ</a>
                    </div>

                    <div class="icon-social-sp kiccho-show-sp">
                        <div class="instagram icon-item">
                            <a href="">
                                <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/instagram.png';?>" alt="instagram">
                            </a>
                        </div>

                        <div class="facebook icon-item">
                            <a href="">
                                <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/facebook.png';?>" alt="facebook">
                            </a>
                        </div>
                    </div>

                    <div class="phone">
                        <a href="tel:0753435298">075-343-5298</a>
                    </div>
                </div>

                <div class="icon-social kiccho-hidden-sp">
                    <div class="instagram icon-item">
                        <a href="">
                            <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/instagram.png';?>" alt="instagram">
                        </a>
                    </div>

                    <div class="facebook icon-item">
                        <a href="">
                            <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/facebook.png';?>" alt="facebook">
                        </a>
                    </div>
                </div>
            </div>
        </header>
        <div class="slideshow-wrap">
            <div class="slide-banner owl-carousel owl-theme kiccho-hidden-sp">
                <div style="background-image: url(<?php echo get_template_directory_uri() .'/assets/images/pc_slide_top/slide1.jpg'; ?>);"></div>
                <div style="background-image: url(<?php echo get_template_directory_uri() .'/assets/images/pc_slide_top/slide2.jpg'; ?>);"></div>
                <div style="background-image: url(<?php echo get_template_directory_uri() .'/assets/images/pc_slide_top/slide3.jpg'; ?>);"></div>
            </div>

            <div class="slide-banner owl-carousel owl-theme kiccho-show-sp">
                <div style="background-image: url(<?php echo get_template_directory_uri() .'/assets/images/sp_slide_top/slide1.jpg'; ?>);"></div>
                <div style="background-image: url(<?php echo get_template_directory_uri() .'/assets/images/sp_slide_top/slide2.jpg'; ?>);"></div>
                <div style="background-image: url(<?php echo get_template_directory_uri() .'/assets/images/sp_slide_top/slide3.jpg'; ?>);"></div>
            </div>
            <div class="slide-paging">
                <p>1</p>
                <span></span>
                <p>3</p>
            </div>

            <div class="logo logo-size kiccho-hidden-sp">
                <?php if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) : ?>
                    <?php the_custom_logo(); ?>
                <?php else : ?> 
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>">
                        <img src="<?php echo get_template_directory_uri() .'/assets/images/logo_white.png' ?>" alt="<?php bloginfo( 'name' ); ?>">
                    </a>
                <?php endif; ?>
            </div>

            <div class="logo-pc kiccho-show-sp">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>">
                    <img src="<?php echo get_template_directory_uri() .'/assets/images/logo_white_pc.png' ?>" alt="<?php bloginfo( 'name' ); ?>">
                </a>
            </div>

            <div class="text-large kiccho-hidden-sp">
                <p>着物ある生活</p>
                <p>日常を華やかに</p>
            </div>

            <div class="text-style text-small kiccho-hidden-sp">
                <a href="#">お問い合わせ</a>
                <a href="#">会社概要</a>
                <a href="#">キモノクリニック</a>
                <a href="#">小物通販</a>
                <a href="#">取扱商品</a>
                <a href="#">振袖特設ペ<span>ー</span>ジ</a>
                <a href="#">お知らせ</a>
                <a href="#">メッセ<span>ー</span>ジ</a>
            </div>
        </div>
    </div>
