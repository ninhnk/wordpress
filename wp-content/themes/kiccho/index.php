<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package kiccho
 */

get_header();
?>
    <main id="main-content">
        <div class="enclosing-block">
            <!-- -----MESSAGE----- -->
            <div class="block-message">
                <div class="body-inner">
                    <div class="img-inner" data-aos="fade-right" data-aos-duration="1500" data-aos-once="true">
                        <img src="<?php echo get_template_directory_uri() .'/assets/images/top_common/img01.jpg' ?>" alt="top_common img01">
                    </div>

                    <div class="block-content" data-aos="fade-right" data-aos-duration="1500" data-aos-once="true">
                        <div class="title-en-ja common-block-title">
                            <label>Message</label>
                            <p>メッセージ</p>
                        </div>
                        <div class="inner-block-content">
                            <h3>日常を華やかに、<span>着物ある生活。</span></h3>
                            <p>智に働けば角が立つ。どこへ越しても住みにくいと悟った時、詩が生れて、画が出来る。とかくに人の世は住みにくい。意地を通せば窮屈だ。 </p>
                            <p>とかくに人の世は住みにくい。</p>
                            <p>どこへ越しても住みにくいと悟った時、詩が生れて、画が出来る。意地を通せば窮屈だ。山路を登りながら、こう考えた。</p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- -----NEWS----- -->
            <div class="block-news">
                <div class="body-inner">
                    <div class="title-en-ja common-block-title">
                        <label>News</label>
                        <p>メッセージ</p>
                    </div>

                    <ul class="list-content" data-aos="fade-right" data-aos-duration="1500" data-aos-once="true">
                        <li class="item-list">
                            <a href="">
                                <span>2020.06.27</span>
                                <p>京都大丸にて展示会のお知らせ</p>
                            </a>
                        </li>

                        <li class="item-list">
                            <a href="">
                                <span>2020.06.27</span>
                                <p>京都大丸にて展示会のお知らせ</p>
                            </a>
                        </li>

                        <li class="item-list">
                            <a href="">
                                <span>2020.06.27</span>
                                <p>京都大丸にて展示会のお知らせ</p>
                            </a>
                        </li>

                        <li class="item-list">
                            <a href="">
                                <span>2020.06.27</span>
                                <p>京都大丸にて展示会のお知らせ</p>
                            </a>
                        </li>

                        <li class="item-list">
                            <a href="">
                                <span>2020.06.27</span>
                                <p>京都大丸にて展示会のお知らせ</p>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="read-more kiccho-hidden-sp">
                    <a href="#">
                        <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/arrow-right.png' ?>" alt="read more">
                        <p>もっと見る</p>
                    </a>
                </div>

                <div class="read-more-pc kiccho-show-sp">
                    <a href="#">もっと見る</a>
                </div>
            </div>

            <!-- -----FURISODE----- -->
            <div class="block-furisode">
                <div class="body-inner">
                    <div class="slide-img" data-aos="fade-in" data-aos-duration="1500" data-aos-once="true">
                        <div class="furisode-slide owl-carousel owl-theme">
                            <div style="background-image: url(<?php echo get_template_directory_uri() .'/assets/images/top_common/inner_slide/slide1.jpg'; ?>);"></div>
                            <div style="background-image: url(<?php echo get_template_directory_uri() .'/assets/images/top_common/inner_slide/slide2.jpg'; ?>);"></div>
                        </div>
                        <div class="slide-paging">
                            <p>1</p>
                            <span></span>
                            <p>3</p>
                        </div>
                    </div>

                    <div class="body-content" data-aos="fade-in" data-aos-duration="1500" data-aos-once="true">
                        <div class="title-en-ja common-block-title">
                            <label>Furisode</label>
                            <p>振袖</p>
                        </div>

                        <div class="text-under-title">
                            <p>成人式を迎えられる方。 ここから始まります。日本の美しさを感じていただく一歩を私たちがお手伝いさせていただきます。</p>
                            <p>一瞬の時だからこそ一生に残る思い出を。</p>
                        </div>

                        <div class="back-to-idx btn-direction">
                            <a href="#" class="kiccho-hidden-sp">一覧に戻る</a>
                            <a href="#" class="kiccho-show-sp">振袖特設ページへ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- -----PRODUCT----- -->
        <div class="block-product">
            <div class="title-en-ja common-block-title">
                <label>Product</label>
                <p>取扱商品</p>
            </div>
        </div>

        <!-- -----MAIN PRODUCT----- -->
        <div class="main-product">
            <div class="title-center">
                <span>Main products</span>
                <span>主な取扱商品</span>
            </div>

            <div class="list-products">
                <div class="surround">
                    <!-- item1 -->
                    <div class="item-product">
                        <div class="img-product">
                            <img src="<?php echo get_template_directory_uri() .'/assets/images/top_common/kimonotype1.jpg' ?>" alt="kimonotype1">
                        </div>

                        <div class="content-pc">
                            <div class="next-to-img">
                                <span>留袖</span>
                                <span>Tome sode</span>
                            </div>
                            <div class="text-under-img">
                                <p>結婚式・式典 格のもっとも高い礼装。</p>
                            </div>
                        </div>
                    </div>
                    <!-- item2 -->
                    <div class="item-product">
                        <div class="img-product">
                            <img src="<?php echo get_template_directory_uri() .'/assets/images/top_common/kimonotype2.jpg' ?>" alt="kimonotype2">
                        </div>
                        
                        <div class="content-pc">
                            <div class="next-to-img">
                                <span>訪問着</span>
                                <span>Semi-formal</span>
                            </div>
                            <div class="text-under-img">
                                <p>結婚式・式典・パーティ おしゃれさとを備えた格の高い訪問着。さまざまなシーンに重宝します。</p>
                            </div>
                        </div>
                    </div>
                    <!-- item3 -->
                    <div class="item-product">
                        <div class="img-product">
                            <img src="<?php echo get_template_directory_uri() .'/assets/images/top_common/kimonotype3.jpg' ?>" alt="kimonotype3">
                        </div>

                        <div class="content-pc">
                            <div class="next-to-img">
                                <span>振袖</span>
                                <span>Furisode</span>
                            </div>
                            <div class="text-under-img">
                                <p>成人式・卒業式・結婚式 未婚女性の礼装。華やかな場に色鮮やかな振袖がいっそう場を盛 り上げます。</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="surround">
                    <!-- item4 -->
                    <div class="item-product">
                        <div class="img-product">
                            <img src="<?php echo get_template_directory_uri() .'/assets/images/top_common/kimonotype4.jpg' ?>" alt="kimonotype4">
                        </div>

                        <div class="content-pc">
                            <div class="next-to-img">
                                <span>紬</span>
                                <span>Tsumugi</span>
                            </div>
                            <div class="text-under-img">
                                <p>パーティ・食事会・観劇 着やすく、カジュアルに、かつおしゃれに。多くの産地からそれぞれ伝統と特徴を持つ紬。個性を生かした着こなしを楽しめる着物。</p>
                            </div>
                        </div>
                    </div>
                    <!-- item5 -->
                    <div class="item-product">
                        <div class="img-product">
                            <img src="<?php echo get_template_directory_uri() .'/assets/images/top_common/kimonotype5.jpg' ?>" alt="kimonotype5">
                        </div>

                        <div class="content-pc">
                            <div class="next-to-img">
                                <span>小紋</span>
                                <span>Komon</span>
                            </div>
                            <div class="text-under-img">
                                <p>食事会・茶会 気軽な、遊び着。カジュアルさで楽しめます。</p>
                            </div>
                        </div>
                    </div>
                    <!-- item6 -->
                    <div class="item-product">
                        <div class="img-product">
                            <img src="<?php echo get_template_directory_uri() .'/assets/images/top_common/kimonotype6.jpg' ?>" alt="kimonotype6">
                        </div>

                        <div class="content-pc">
                            <div class="next-to-img">
                                <span>男性着物</span>
                                <span>Men’ s</span>
                            </div>
                            <div class="text-under-img">
                                <p>大島紬、お召など パーティや食事会。茶会。集まり。着こなしが男の魅力です。</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- -----OTHER PRODUCT----- -->
            <div class="other-product">
                <div class="next-to-bgc">
                    <span>その他の取扱商品</span>
                    <span>Other products</span>
                </div>

                <div class="list-img">
                    <div class="inner-list">
                        <div class="item-img">
                            <img src="<?php echo get_template_directory_uri() .'/assets/images/top_common/otherproduct1.jpg' ?>" alt="otherproduct1">
                        </div>
                        <div class="text-under-img">
                            <p>お召</p>
                        </div>
                    </div>
                    <div class="inner-list">
                        <div class="item-img">
                            <img src="<?php echo get_template_directory_uri() .'/assets/images/top_common/otherproduct2.jpg' ?>" alt="otherproduct2">
                        </div>
                        <div class="text-under-img">
                            <p>帯締</p>
                        </div>
                    </div>
                    <div class="inner-list">
                        <div class="item-img">
                            <img src="<?php echo get_template_directory_uri() .'/assets/images/top_common/otherproduct3.jpg' ?>" alt="otherproduct3">
                        </div>
                        <div class="text-under-img">
                            <p>和装小物</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block-under-other">
                <p>その他お襦袢や和服に関わる商品を取り扱いしております。</p>
                <p>詳しくはお問い合わせください。</p>
            </div>
        </div>

        <!-- -----KIMONO CLINIC----- -->
        <div class="kimono-clinic">
            <div class="wrap-body">
                <div class="wrap-content">
                    <div class="content-left">
                        <div class="inner-img">
                            <img src="<?php echo get_template_directory_uri() .'/assets/images/top_common/img02.jpg' ?>" alt="img02">
                        </div>

                        <div class="text-next-to-bgc">
                            <p>キモノクリニック</p>
                        </div>
                    </div>

                    <div class="content-right">
                        <div class="text-title kiccho-hidden-sp">
                            <h3>キモノクリニックのコピー</h3>
                        </div>

                        <div class="text-content">
                            <p>情に棹させば流される。智に働けば角が立つ。どこへ越しても住みにくいと悟った時、詩が生れて、画が出来る。とかくに人の世は住みにくい。意地を通せば窮屈だ</p>
                            <p>とかくに人の世は住みにくい。</p>
                            <p>どこへ越しても住みにくいと悟った時、詩が生れて、画が出来る。意地を通せば窮屈だ。山路を登りながら、こう考えた。智に働けば角が立つ。どこへ越しても住みにくいと悟った時、詩が生れて、画が出来る。智に働けば角が立つ。 </p>
                        </div>
                        <div class="btn-direction btn-clinic">
                            <a href="">お問い合わせ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- -----KIMONO ACCESORIES----- -->
        <div class="kimono-accessories">
            <div class="section-title">
                <div class="block-left kiccho-hidden-sp">
                    <img src="<?php echo get_template_directory_uri() .'/assets/images/top_common/accessories1.jpg' ?>" alt="accessories1">
                </div>
                <div class="block-left kiccho-show-sp">
                    <img src="<?php echo get_template_directory_uri() .'/assets/images/top_common/accessories3.jpg' ?>" alt="accessories3">
                </div>
                <div class="block-mid">
                    <div class="inner-text">
                        <span>小物をあなたのもとへ</span>
                        <span>オンラインで気軽に</span>
                        <span>和装小物</span>
                    </div>
                </div>
                <div class="block-right kiccho-hidden-sp">
                    <img src="<?php echo get_template_directory_uri() .'/assets/images/top_common/accessories2.jpg' ?>" alt="accessories1">
                </div>
            </div>

            <div class="section-product">
                <div class="list-products">
                    <div class="surround">
                        <!-- item1 -->
                        <div class="item-product">
                            <div class="block-hover">
                                <div class="inner-img">
                                    <img src="<?php echo get_template_directory_uri() .'/assets/images/products/item-product1.jpg' ?>" alt="item-product1">
                                </div>
                                <div class="hover-item">
                                    <div class="inner-hover">
                                        <div class="icon-cart">
                                            <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                        </div>
                                        <div class="price">
                                            <span>¥3,000</span>
                                        </div>
                                        <div class="btn-shop-cart">
                                            <a href="">詳細を見る</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-under">
                                <span>日本製レース半衿（七宝柄</span>
                            </div>
                            <div class="price-pc">
                                <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                <span>¥3,000</span>
                            </div>

                        </div>

                        <!-- item2 -->
                        <div class="item-product">
                            <div class="block-hover">
                                <div class="inner-img">
                                    <img src="<?php echo get_template_directory_uri() .'/assets/images/products/item-product2.jpg' ?>" alt="item-product2">
                                </div>
                                <div class="hover-item">
                                    <div class="inner-hover">
                                        <div class="icon-cart">
                                            <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                        </div>
                                        <div class="price">
                                            <span>¥3,000</span>
                                        </div>
                                        <div class="btn-shop-cart">
                                            <a href="">詳細を見る</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-under">
                                <span>日本製レース半衿（七宝柄</span>
                            </div>
                            <div class="price-pc">
                                <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                <span>¥3,000</span>
                            </div>
                        </div>

                        <!-- item3 -->
                        <div class="item-product">
                            <div class="block-hover">
                                <div class="inner-img">
                                    <img src="<?php echo get_template_directory_uri() .'/assets/images/products/item-product3.jpg' ?>" alt="item-product3">
                                </div>
                                <div class="hover-item">
                                    <div class="inner-hover">
                                        <div class="icon-cart">
                                            <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                        </div>
                                        <div class="price">
                                            <span>¥3,000</span>
                                        </div>
                                        <div class="btn-shop-cart">
                                            <a href="">詳細を見る</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-under">
                                <span>日本製レース半衿（七宝柄</span>
                            </div>
                            <div class="price-pc">
                                <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                <span>¥3,000</span>
                            </div>
                        </div>

                        <!-- item4 -->
                        <div class="item-product">
                            <div class="block-hover">
                                <div class="inner-img">
                                    <img src="<?php echo get_template_directory_uri() .'/assets/images/products/item-product4.jpg' ?>" alt="item-product4">
                                </div>
                                <div class="hover-item">
                                    <div class="inner-hover">
                                        <div class="icon-cart">
                                            <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                        </div>
                                        <div class="price">
                                            <span>¥3,000</span>
                                        </div>
                                        <div class="btn-shop-cart">
                                            <a href="">詳細を見る</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-under">
                                <span>日本製レース半衿（七宝柄</span>
                            </div>
                            <div class="price-pc">
                                <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                <span>¥3,000</span>
                            </div>
                        </div>

                        <!-- item5 -->
                        <div class="item-product">
                            <div class="block-hover">
                                <div class="inner-img">
                                    <img src="<?php echo get_template_directory_uri() .'/assets/images/products/item-product1.jpg' ?>" alt="item-product1">
                                </div>
                                <div class="hover-item">
                                    <div class="inner-hover">
                                        <div class="icon-cart">
                                            <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                        </div>
                                        <div class="price">
                                            <span>¥3,000</span>
                                        </div>
                                        <div class="btn-shop-cart">
                                            <a href="">詳細を見る</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-under">
                                <span>日本製レース半衿（七宝柄</span>
                            </div>
                            <div class="price-pc">
                                <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                <span>¥3,000</span>
                            </div>
                        </div>
                    </div>

                    <div class="surround">
                        <!-- item1 -->
                        <div class="item-product">
                            <div class="block-hover">
                                <div class="inner-img">
                                    <img src="<?php echo get_template_directory_uri() .'/assets/images/products/item-product1.jpg' ?>" alt="item-product1">
                                </div>
                                <div class="hover-item">
                                    <div class="inner-hover">
                                        <div class="icon-cart">
                                            <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                        </div>
                                        <div class="price">
                                            <span>¥3,000</span>
                                        </div>
                                        <div class="btn-shop-cart">
                                            <a href="">詳細を見る</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-under">
                                <span>日本製レース半衿（七宝柄</span>
                            </div>
                            <div class="price-pc">
                                <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                <span>¥3,000</span>
                            </div>

                        </div>

                        <!-- item2 -->
                        <div class="item-product">
                            <div class="block-hover">
                                <div class="inner-img">
                                    <img src="<?php echo get_template_directory_uri() .'/assets/images/products/item-product2.jpg' ?>" alt="item-product2">
                                </div>
                                <div class="hover-item">
                                    <div class="inner-hover">
                                        <div class="icon-cart">
                                            <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                        </div>
                                        <div class="price">
                                            <span>¥3,000</span>
                                        </div>
                                        <div class="btn-shop-cart">
                                            <a href="">詳細を見る</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-under">
                                <span>日本製レース半衿（七宝柄</span>
                            </div>
                            <div class="price-pc">
                                <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                <span>¥3,000</span>
                            </div>
                        </div>

                        <!-- item3 -->
                        <div class="item-product">
                            <div class="block-hover">
                                <div class="inner-img">
                                    <img src="<?php echo get_template_directory_uri() .'/assets/images/products/item-product3.jpg' ?>" alt="item-product3">
                                </div>
                                <div class="hover-item">
                                    <div class="inner-hover">
                                        <div class="icon-cart">
                                            <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                        </div>
                                        <div class="price">
                                            <span>¥3,000</span>
                                        </div>
                                        <div class="btn-shop-cart">
                                            <a href="">詳細を見る</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-under">
                                <span>日本製レース半衿（七宝柄</span>
                            </div>
                            <div class="price-pc">
                                <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                <span>¥3,000</span>
                            </div>
                        </div>

                        <!-- item4 -->
                        <div class="item-product">
                            <div class="block-hover">
                                <div class="inner-img">
                                    <img src="<?php echo get_template_directory_uri() .'/assets/images/products/item-product4.jpg' ?>" alt="item-product4">
                                </div>
                                <div class="hover-item">
                                    <div class="inner-hover">
                                        <div class="icon-cart">
                                            <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                        </div>
                                        <div class="price">
                                            <span>¥3,000</span>
                                        </div>
                                        <div class="btn-shop-cart">
                                            <a href="">詳細を見る</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-under">
                                <span>日本製レース半衿（七宝柄</span>
                            </div>
                            <div class="price-pc">
                                <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                <span>¥3,000</span>
                            </div>
                        </div>

                        <!-- item5 -->
                        <div class="item-product">
                            <div class="block-hover">
                                <div class="inner-img">
                                    <img src="<?php echo get_template_directory_uri() .'/assets/images/products/item-product1.jpg' ?>" alt="item-product1">
                                </div>
                                <div class="hover-item">
                                    <div class="inner-hover">
                                        <div class="icon-cart">
                                            <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                        </div>
                                        <div class="price">
                                            <span>¥3,000</span>
                                        </div>
                                        <div class="btn-shop-cart">
                                            <a href="">詳細を見る</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-under">
                                <span>日本製レース半衿（七宝柄</span>
                            </div>
                            <div class="price-pc">
                                <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/cart.png' ?>" alt="cart">
                                <span>¥3,000</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="read-more">
                    <a href="#">
                        <span>もっと見る</span>
                        <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/arrow-down.png' ?>" alt="read more">
                    </a>
                </div>
            </div>
        </div>

        <!-- -----CONTACT FORM----- -->
        <div class="contact-form">
            <div class="wrap-body">
                <div class="contact-us">
                    <div class="contact-title">
                        <p>お問い合わせ</p>
                    </div>

                    <form action="" class="form">
                        <div class="itemt-input">
                            <input type="text" name="" placeholder="名前">
                        </div>
                        <div class="itemt-input">
                            <input type="text" name="" placeholder="電話番号">
                        </div>
                        <div class="itemt-input">
                            <input type="text" name="" placeholder="お問い合わせ種別">
                        </div>
                        <div class="itemt-input">
                            <textarea name="" placeholder="内容" cols="30" rows="10"></textarea>
                        </div>

                        <div class="btn-submit btn-direction">
                            <a href="#">送信</a>
                        </div>
                    </form>
                </div>
                <div class="cpn-profile">
                    <div class="cpn-title">
                        <p>会社概要</p>
                    </div>
                    <div class="cpn-content">
                        <div class="item-inner">
                            <p>商号</p>
                            <p>株式会社 吉兆</p>
                        </div>
                        <div class="item-inner">
                            <p>設立</p>
                            <p>2222年12月22日</p>
                        </div>
                        <div class="item-inner">
                            <p>事業内容</p>
                            <p>着物の販売</p>
                        </div>
                        <div class="item-inner">
                            <p>資本金</p>
                            <p>０００万円</p>
                        </div>
                        <div class="item-inner">
                            <p>本社所在地</p>
                            <p>京都市下京区不明門通五条上る玉屋町513</p>
                        </div>
                        <div class="item-inner">
                            <p>電話番号</p>
                            <p>075-343-5298</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
get_footer();
