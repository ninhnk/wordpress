<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kiccho
 */

?>
    <div id="gmap"></div>
    <div class="back-to-top">
        <img src="<?php echo get_template_directory_uri() .'/assets/images/icon/back-to-top.png';?>" alt="back to top">
    </div>
    <footer id="footer">
        <div class="text-style text-footer kiccho-hidden-sp">
            <a href="#">プライバシ<span>ー</span>ポリシ<span>ー</span></a>
            <a href="#">お問い合わせ</a>
            <a href="#">会社概要</a>
            <a href="#">キモノクリニック</a>
            <a href="#">小物通販</a>
            <a href="#">取扱商品</a>
            <a href="#">振袖特設ペ<span>ー</span>ジ</a>
            <a href="#">お知らせ</a>
            <a href="#">メッセ<span>ー</span>ジ</a>
        </div>

        <div class="logo-size logo-footer">
            <?php if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) : ?>
                <?php the_custom_logo(); ?>
            <?php else : ?> 
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>">
                    <img src="<?php echo get_template_directory_uri() .'/assets/images/logo_white.png' ?>" alt="<?php bloginfo( 'name' ); ?>">
                </a>
            <?php endif; ?>
        </div>
    </footer>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
<script>
    function initMap() {
        const map = new google.maps.Map(document.getElementById("gmap"), {
            zoom: 17,
            center: {
                lat: 34.997141,
                lng: 135.760242
            }
        });
        const image = "<?php echo get_template_directory_uri() .'/assets/images/icon/pin.png' ?>";
        const beachMarker = new google.maps.Marker({
            position: {
                lat: 34.997141,
                lng: 135.760242
            },
            map,
            icon: image
        });
    }
</script>
