var Common = function() {
    var backToTop = function () {
        var element = $('.back-to-top');
        var height = $('.text-large').height();
        $(window).scroll(function() {
            if ($(window).scrollTop() > height) {
                element.addClass('show');
            } else {
                element.removeClass('show');
            }
        });

        element.on('click', function (event) {
            event.preventDefault();
            $('html, body').animate({scrollTop:0}, '300');
        });
    };

    var slideOwlCarousel = function () {
        let elementSlide = $('.slide-banner');
        if (elementSlide) {
            elementSlide.owlCarousel({
                loop: true,
                nav:false,
                items:1,
                dots: false,

            });
        }
    };

    var furisodeSlide = function () {
        let elementSlide = $('.furisode-slide');
        if (elementSlide) {
            elementSlide.owlCarousel({
                loop: true,
                nav:false,
                items:1,
                dots: false,

            });
        }
    };

    var clickNavMenu = function () {
        $('.nav-menu').click(function () {
            $(this).toggleClass('change');
            $('body').toggleClass('overflow-hidden');
            $('.open-nav').toggleClass('open-nav--mod');
        });
    }

    return {
        init: function () {
            backToTop();
            slideOwlCarousel();
            furisodeSlide();
            clickNavMenu();
        }
    }
}();

$(function () {
    Common.init();
    AOS.init();
});
